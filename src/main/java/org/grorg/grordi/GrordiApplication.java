package org.grorg.grordi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({
        "org.grorg.grordi.config",
        "org.grorg.grordi.service",
        "org.grorg.grordi.controller"
})
@SpringBootApplication
public class GrordiApplication {
    /**
     * Setup the application.
     * @param args Application parameters
     */
    public static void main(String[] args) {
        SpringApplication.run(GrordiApplication.class, args);
    }
}
