package org.grorg.grordi.dto;

import org.grorg.grordi.entity.Computer;

import java.time.format.DateTimeFormatter;

public final class ComputerWithCompanyIdDtoFactory {
    /**
     * ComputerWithCompanyIdDtoFactory is an helper static class, so we cannot instantiate it.
     * @throws IllegalAccessException If we try to instantiate by reflexion
     */
    private ComputerWithCompanyIdDtoFactory() throws IllegalAccessException {
        throw new IllegalAccessException("ComputerWithCompanyIdDtoFactory is static so not instantiable");
    }

    /**
     * Transform the computer given in parameter to ComputerWithCompanyIdDto.
     * @param computer The computer to transform
     * @return The computer given in parameter transformed in ComputerWithCompanyIdDto
     */
    public static ComputerWithCompanyIdDto fromComputerToDto(Computer computer) {
        final DateTimeFormatter dateFormatter = ComputerWithCompanyIdDto.DATE_FORMAT;
        ComputerWithCompanyIdDto result = new ComputerWithCompanyIdDto();
        result.setId(computer.getId());
        result.setName(computer.getName());
        result.setIntroduced(Util.fromLocalDateToStringDate(dateFormatter, computer.getIntroduced()));
        result.setDiscontinued(Util.fromLocalDateToStringDate(dateFormatter, computer.getDiscontinued()));
        if (computer.getCompany() != null) {
            result.setCompany(computer.getCompany().getId());
        }
        return result;
    }

    /**
     * Transform the computer dto into Computer.
     * <b>The returned computer will not has company information</b>
     * @param computer The dto to transform
     * @return The transformed dto in Computer
     */
    public static Computer fromDtoToComputer(ComputerWithCompanyIdDto computer) {
        final DateTimeFormatter dateFormatter = ComputerWithCompanyIdDto.DATE_FORMAT;
        Computer result = new Computer();
        result.setId(computer.getId());
        result.setName(computer.getName());
        result.setIntroduced(Util.fromStringDateToLocalDate(dateFormatter, computer.getIntroduced()));
        result.setDiscontinued(Util.fromStringDateToLocalDate(dateFormatter, computer.getDiscontinued()));
        return result;
    }
}
