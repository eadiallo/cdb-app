package org.grorg.grordi.dto;

import org.grorg.grordi.dto.error.Error;
import org.grorg.grordi.dto.error.Errors;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

public final class ComputerWithCompanyIdDtoValidator {
    /**
     * ComputerWithCompanyIdDtoValidator is a static helper class and should not be instantiate.
     * @throws IllegalAccessException When we try to instantiate this class via reflexion
     */
    private ComputerWithCompanyIdDtoValidator() throws IllegalAccessException {
        throw new IllegalAccessException("ComputerWithCompanyIdDtoValidator should not be instantiate");
    }

    /**
     * Validate the ComputerWithCompanyIdDto given in parameter, if there are errors they are return as list.
     * @param computer The computer to validate
     * @return The list of errors if there are, an empty list otherwise
     */
    public static List<Error> validate(ComputerWithCompanyIdDto computer) {
        List<Error> errors = new ArrayList<>();
        if (computer == null) {
            errors.add(Errors.COMPUTER_SHOULD_NOT_BE_EMPTY);
            return errors;
        }

        if (computer.getName() == null || "".equals(computer.getName())) {
            errors.add(Errors.COMPUTER_NAME_SHOULD_NOT_BE_EMPTY);
        }

        LocalDate introduced = null;
        LocalDate discontinued = null;
        if (computer.getIntroduced() != null) {
            try {
                introduced = LocalDate.parse(computer.getIntroduced(), ComputerWithCompanyIdDto.DATE_FORMAT);
            } catch (DateTimeParseException e) {
                errors.add(Errors.COMPUTER_INTRODUCED_DATE_SHOULD_BE_IN_ISO_FORMAT);
            }
        }
        if (computer.getDiscontinued() != null) {
            try {
                discontinued = LocalDate.parse(computer.getDiscontinued(), ComputerWithCompanyIdDto.DATE_FORMAT);
            } catch (DateTimeParseException e) {
                errors.add(Errors.COMPUTER_DISCONTINUED_DATE_SHOULD_BE_IN_ISO_FORMAT);
            }
        }
        if (introduced != null && discontinued != null && introduced.isAfter(discontinued)) {
            errors.add(Errors.COMPUTER_INTRODUCED_DATE_SHOULD_BE_BEFORE_DISCONTINUED_DATE);
        }

        return errors;
    }

    /**
     * Validate the ComputerWithCompanyIdDto given in parameter for a persisting operation. If there is error the return
     * list will contains all errors. Otherwise it will be empty.
     * @param computer The computer to validate
     * @return The error list or an empty list if there is no error
     */
    public static List<Error> validateForCreation(ComputerWithCompanyIdDto computer) {
        List<Error> errors = validate(computer);
        if (computer != null && computer.getId() != null) {
            errors.add(Errors.COMPUTER_TO_CREATE_SHOULD_NOT_HAVE_ID);
        }
        return errors;
    }
}
