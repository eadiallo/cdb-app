package org.grorg.grordi.dto;

import java.util.Objects;

public final class ComputerWithCompanyNameDto extends ComputerDto {
    private String company;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ComputerWithCompanyNameDto that = (ComputerWithCompanyNameDto) o;
        return Objects.equals(company, that.company);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "ComputerWithCompanyNameDto{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", introduced='" + getIntroduced() + '\'' +
                ", discontinued='" + getDiscontinued() + '\'' +
                ", company='" + company + '\'' +
                '}';
    }
}
