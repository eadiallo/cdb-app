package org.grorg.grordi.service;

import org.grorg.grordi.dao.ComputerDao;
import org.grorg.grordi.dao.OperationDao;
import org.grorg.grordi.dto.Page;
import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.entity.Computer;
import org.grorg.grordi.entity.Operation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class ComputerService {
    private final ComputerDao computerDao;
    private final OperationDao operationDao;

    /**
     * Creates new computer service depending computer dao and operation dao.
     * @param computerDao Data object access to computer used in services
     * @param operationDao Data object access to log operations when insert/update/delete is performed on computer
     */
    public ComputerService(ComputerDao computerDao, OperationDao operationDao) {
        this.computerDao = computerDao;
        this.operationDao = operationDao;
    }

    /**
     * Persist computer in the application and modify computer given in parameter to assign it its new id.
     * Given computer to persist must has been validated.
     *
     * @param computer Computer to persist that has been validated for creation
     * @return The created computer filled with creation information (typically its creation id)
     */
    @Transactional
    public Computer createComputer(ValidForCreation<Computer> computer) {
        Computer computerToCreate = computer.get();
        computerDao.create(computerToCreate);
        operationDao.create(
                Operation.builder()
                        .forComputer()
                        .withEntityId(computerToCreate.getId())
                        .ofInsertionType()
                        .build());
        return computerToCreate;
    }

    /**
     * Find computer by its id and return it.
     *
     * @param id The computer id
     * @return The computer instance if found, null otherwise
     */
    public Computer findComputerById(long id) {
        return computerDao.findById(id);
    }

    /**
     * Performs find computers by given valid criteria.
     * @param criteria The search criteria that have been validated
     * @return The search result as page object
     */
    @Transactional
    public Page<Computer> findComputerByCriteria(Valid<SearchCriteria<Computer>> criteria) {
        final Collection<Computer> foundComputers = computerDao.findByCriteria(criteria.get());
        final long totalNumberOfFoundElements = computerDao.getTotalNumberOfFoundElementsByCriteria(criteria.get());
        return Page.fromSearchCriteriaAndResult(criteria.get(), foundComputers, totalNumberOfFoundElements);
    }

    /**
     * Update a computer that as been validated for the update.
     * @param validComputer A valid computer to update
     */
    @Transactional
    public void update(ValidForUpdate<Computer> validComputer) {
        final Computer computer = validComputer.get();
        if (computerDao.exists(computer.getId())) {
            computerDao.update(computer);
            operationDao.create(
                    Operation.builder()
                            .forComputer()
                            .withEntityId(computer.getId())
                            .ofUpdatingType()
                            .build());
        }
    }

    /**
     * Delete computer with the given id.
     * @param id The id of computer to delete
     */
    @Transactional
    public void deleteComputerWithId(long id) {
        computerDao.delete(id);
        operationDao.create(
                Operation.builder()
                        .forComputer()
                        .withEntityId(id)
                        .ofDeletionType()
                        .build());
    }
}
