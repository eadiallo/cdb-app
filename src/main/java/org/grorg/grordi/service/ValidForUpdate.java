package org.grorg.grordi.service;

public final class ValidForUpdate<T> extends AbstractValid<T> {
    /**
     * Validates the element given in parameter for an update operation.
     * @param validElement Element to validate
     */
    ValidForUpdate(T validElement) {
        super(validElement);
    }
}
