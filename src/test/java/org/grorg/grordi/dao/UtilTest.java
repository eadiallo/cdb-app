package org.grorg.grordi.dao;

import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import org.grorg.grordi.dto.SearchCriteria;
import org.grorg.grordi.entity.Computer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UtilTest {
    private static final String SORTING_COLUMN = "name";
    private static final String DEFAULT_SORTING_COLUMN = "id";
    private static final String ENTITY_PREFIX = "computer.";
    private static final String ENTITY_SORTING_COLUMN = ENTITY_PREFIX + SORTING_COLUMN;
    private static final String ENTITY_DEFAULT_SORTING_COLUMN = ENTITY_PREFIX + DEFAULT_SORTING_COLUMN;


    @Test
    void getOrderSpecifierShouldReturnGoodOrderSpecifierBasedOnTheCriteria() {
        SearchCriteria<Computer> criteria = SearchCriteria
                .builder(Computer.class)
                .sortingColumn(SORTING_COLUMN)
                .sortingOrientation(SearchCriteria.SortingOrientation.DESCENDING)
                .build();

        OrderSpecifier orderSpecifier = Util.getOrderSpecifierOrDefault(criteria, DEFAULT_SORTING_COLUMN);
        assertEquals(Order.DESC, orderSpecifier.getOrder());
        assertEquals(ENTITY_SORTING_COLUMN, orderSpecifier.getTarget().toString());
    }

    @Test
    void getOrderSpecifierShouldReturnGoodOrderSpecifierForAscendingCriteria() {
        SearchCriteria<Computer> criteria = SearchCriteria
                .builder(Computer.class)
                .sortingColumn(SORTING_COLUMN)
                .sortingOrientation(SearchCriteria.SortingOrientation.ASCENDING)
                .build();

        OrderSpecifier orderSpecifier = Util.getOrderSpecifierOrDefault(criteria, DEFAULT_SORTING_COLUMN);
        assertEquals(Order.ASC, orderSpecifier.getOrder());
        assertEquals(ENTITY_SORTING_COLUMN, orderSpecifier.getTarget().toString());
    }

    @Test
    void getOrderSpecifierShouldReturnDefaultOrderSpecifierForAscending() {
        SearchCriteria<Computer> criteria = SearchCriteria
                .builder(Computer.class)
                .build();

        OrderSpecifier orderSpecifier = Util.getOrderSpecifierOrDefault(criteria, DEFAULT_SORTING_COLUMN);
        assertEquals(Order.ASC, orderSpecifier.getOrder());
        assertEquals(ENTITY_DEFAULT_SORTING_COLUMN, orderSpecifier.getTarget().toString());
    }

    @Test
    void getOrderSpecifierShouldReturnDefaultOrderSpecifierForDescending() {
        SearchCriteria<Computer> criteria = SearchCriteria
                .builder(Computer.class)
                .sortingOrientation(SearchCriteria.SortingOrientation.DESCENDING)
                .build();

        OrderSpecifier orderSpecifier = Util.getOrderSpecifierOrDefault(criteria, DEFAULT_SORTING_COLUMN);
        assertEquals(Order.DESC, orderSpecifier.getOrder());
        assertEquals(ENTITY_DEFAULT_SORTING_COLUMN, orderSpecifier.getTarget().toString());
    }
}
