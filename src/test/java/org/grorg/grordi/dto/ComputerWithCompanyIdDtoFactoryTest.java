package org.grorg.grordi.dto;

import org.grorg.grordi.entity.Computer;
import org.grorg.grordi.util.ComputerDtoFactory;
import org.grorg.grordi.util.ComputerFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ComputerWithCompanyIdDtoFactoryTest {
    @Test
    void computerShouldBeTransformedInComputerWithCompanyIdDto() {
        assertEquals(ComputerDtoFactory.newFirstComputerInDatabase(),
                ComputerWithCompanyIdDtoFactory.fromComputerToDto(ComputerFactory.newFirstComputerInDatabase()));
    }

    @Test
    void computerWithDateShouldCorrectlyTransformedInComputerWithCompanyIdDto() {
        assertEquals(ComputerDtoFactory.newComputerWithDateDto(),
                ComputerWithCompanyIdDtoFactory.fromComputerToDto(ComputerFactory.newComputerWithDates()));
    }

    @Test
    void computerWithCompanyIdDtoShouldBeTransformedInComputerWithoutCompany() {
        Computer firstComputerInDatabaseWithoutCompany = ComputerFactory.newFirstComputerInDatabase();
        firstComputerInDatabaseWithoutCompany.setCompany(null);
        assertEquals(firstComputerInDatabaseWithoutCompany,
                ComputerWithCompanyIdDtoFactory.fromDtoToComputer(ComputerDtoFactory.newFirstComputerInDatabase()));
    }

    @Test
    void computerWithCompanyIdAndDateShouldCorrectlyBeTransformedInComputerWithoutCompany() {
        Computer computerWithDateButCompany = ComputerFactory.newComputerWithDates();
        computerWithDateButCompany.setCompany(null);
        assertEquals(computerWithDateButCompany,
                ComputerWithCompanyIdDtoFactory.fromDtoToComputer(ComputerDtoFactory.newComputerWithDateDto()));
    }
}
