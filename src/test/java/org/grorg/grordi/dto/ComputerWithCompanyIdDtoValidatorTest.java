package org.grorg.grordi.dto;

import org.grorg.grordi.dto.error.Errors;
import org.grorg.grordi.util.ComputerDtoFactory;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ComputerWithCompanyIdDtoValidatorTest {
    @Test
    void validComputerShouldBeValidate() {
        assertEquals(Collections.emptyList(),
                ComputerWithCompanyIdDtoValidator.validate(ComputerDtoFactory.newValidComputer()));
        assertEquals(Collections.emptyList(),
                ComputerWithCompanyIdDtoValidator.validate(ComputerDtoFactory.newValidWithDateComputer()));
    }

    @Test
    void nullComputerShouldReturnError() {
        assertEquals(Collections.singletonList(Errors.COMPUTER_SHOULD_NOT_BE_EMPTY),
                ComputerWithCompanyIdDtoValidator.validate(null));
    }

    @Test
    void computerWithNoNameShouldReturnError() {
        assertEquals(Collections.singletonList(Errors.COMPUTER_NAME_SHOULD_NOT_BE_EMPTY),
                ComputerWithCompanyIdDtoValidator.validate(ComputerDtoFactory.newInvalidComputerWithNullName()));
        assertEquals(Collections.singletonList(Errors.COMPUTER_NAME_SHOULD_NOT_BE_EMPTY),
                ComputerWithCompanyIdDtoValidator.validate(ComputerDtoFactory.newInvalidComputerWithEmptyName()));
    }

    @Test
    void computerWithInvalidIntroductionDateShouldReturnError() {
        assertEquals(Collections.singletonList(Errors.COMPUTER_INTRODUCED_DATE_SHOULD_BE_IN_ISO_FORMAT),
                ComputerWithCompanyIdDtoValidator.validate(ComputerDtoFactory
                        .newInvalidComputerWithInvalidIntroductionDate()));
    }

    @Test
    void computerWithInvalidDiscontinuedDateShouldReturnError() {
        assertEquals(Collections.singletonList(Errors.COMPUTER_DISCONTINUED_DATE_SHOULD_BE_IN_ISO_FORMAT),
                ComputerWithCompanyIdDtoValidator.validate(ComputerDtoFactory
                        .newInvalidComputerWithInvalidDiscontinuedDate()));
    }

    @Test
    void computerWithInvalidDatesShouldReturnErrors() {
        assertEquals(Arrays.asList(Errors.COMPUTER_INTRODUCED_DATE_SHOULD_BE_IN_ISO_FORMAT,
                Errors.COMPUTER_DISCONTINUED_DATE_SHOULD_BE_IN_ISO_FORMAT),
                ComputerWithCompanyIdDtoValidator.validate(ComputerDtoFactory.newInvalidComputerWithInvalidDates()));
    }

    @Test
    void computerWithIntroductionDateAfterDiscontinuedDateShouldReturnError() {
        assertEquals(Collections.singletonList(Errors.COMPUTER_INTRODUCED_DATE_SHOULD_BE_BEFORE_DISCONTINUED_DATE),
                ComputerWithCompanyIdDtoValidator.validate(
                        ComputerDtoFactory.newInvalidComputerWithIntroducedAfterDiscontinued()));
    }

    @Test
    void computerWithMultipleErrorsShouldReturnErrors() {
        assertEquals(Arrays.asList(Errors.COMPUTER_NAME_SHOULD_NOT_BE_EMPTY,
                Errors.COMPUTER_INTRODUCED_DATE_SHOULD_BE_IN_ISO_FORMAT,
                Errors.COMPUTER_DISCONTINUED_DATE_SHOULD_BE_IN_ISO_FORMAT),
                ComputerWithCompanyIdDtoValidator.validate(
                        ComputerDtoFactory.newInvalidComputerWithInvalidDatesAndName()));
    }

    @Test
    void validComputerWithoutIdShouldBeValidateForCreation() {
        assertEquals(Collections.emptyList(), ComputerWithCompanyIdDtoValidator.validateForCreation(
                ComputerDtoFactory.newComputerWithIntroducedDateWithoutId()));
    }

    @Test
    void validComputerWithIdShouldReturnErrorOnValidateForCreation() {
        assertEquals(Collections.singletonList(Errors.COMPUTER_TO_CREATE_SHOULD_NOT_HAVE_ID),
                ComputerWithCompanyIdDtoValidator.validateForCreation(ComputerDtoFactory.newValidComputer()));
    }
}
