package org.grorg.grordi.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ValidStateTest {
    @Test
    void validAndValidForCreationShouldNotBeEqualsOnTheSameObject() {
        final Object obj = new Object();
        assertNotEquals(new Valid<>(obj), new ValidForCreation<>(obj));
    }

    @Test
    void validShouldBeEqualsWithTheSameValidObject() {
        final Object obj = new Object();
        assertEquals(new Valid<>(obj), new Valid<>(obj));
    }

    @Test
    void validShouldNotBeEqualsWithDifferentObject() {
        final Object obj = new Object();
        final Object differentObject = new Object();
        assertNotEquals(new Valid<>(obj), new Valid<>(differentObject));
    }

    @Test
    void validShouldHaveToStringWithValidName() {
        assertEquals("Valid(test)", new Valid<>("test").toString());
    }

    @Test
    void validForCreationShouldHaveToStringWithValidForCreationName() {
        assertEquals("ValidForCreation(test)", new ValidForCreation<>("test").toString());
    }
}
