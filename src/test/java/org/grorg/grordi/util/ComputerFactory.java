package org.grorg.grordi.util;

import org.grorg.grordi.entity.Computer;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ComputerFactory {
    public static Computer newFirstComputerInDatabase() {
        return newMacBookPro15();
    }

    public static Computer newValidComputerWithoutId() {
        return Computer.builder()
                .name(Consts.Computer.Valid.NAME)
                .company(CompanyFactory.newValidCompany())
                .build();
    }

    public static Computer newValidAppleComputerWithoutId() {
        return Computer.builder()
                .name(Consts.Computer.Valid.NAME)
                .company(CompanyFactory.newAppleCompany())
                .build();
    }

    public static Computer newComputerWithIntroducedDateWithoutId() {
        return Computer.builder()
                .name(Consts.Computer.Valid.NAME)
                .introduced(Consts.Computer.Valid.INTRODUCED)
                .company(Consts.Computer.Valid.COMPANY)
                .build();
    }

    public static Computer newValidAppleComputerWithoutIdWithDates() {
        return Computer.builder()
                .name(Consts.Computer.Valid.NAME)
                .introduced(Consts.Computer.Valid.INTRODUCED)
                .discontinued(Consts.Computer.Valid.DISCONTINUED)
                .company(CompanyFactory.newAppleCompany())
                .build();
    }

    public static Computer newValidComputer() {
        return Computer.builder()
                .id(Consts.Computer.Valid.ID)
                .name(Consts.Computer.Valid.NAME)
                .company(CompanyFactory.newValidCompany())
                .build();
    }

    public static Computer newComputerWithDates() {
        return Computer.builder()
                .id(Consts.Computer.Valid.ID)
                .name(Consts.Computer.Valid.NAME)
                .introduced(Consts.Computer.Valid.INTRODUCED)
                .discontinued(Consts.Computer.Valid.DISCONTINUED)
                .company(Consts.Computer.Valid.COMPANY)
                .build();
    }
    
    public static Computer newMacBookPro15() {
        return Computer.builder()
                .id(Consts.Computer.MacBookPro15.ID)
                .name(Consts.Computer.MacBookPro15.NAME)
                .introduced(Consts.Computer.MacBookPro15.INTRODUCED)
                .discontinued(Consts.Computer.MacBookPro15.DISCONTINUED)
                .company(Consts.Computer.MacBookPro15.COMPANY)
                .build();
    }

    public static Computer newCm2a() {
        return Computer.builder()
                .id(Consts.Computer.Cm2a.ID)
                .name(Consts.Computer.Cm2a.NAME)
                .introduced(Consts.Computer.Cm2a.INTRODUCED)
                .discontinued(Consts.Computer.Cm2a.DISCONTINUED)
                .company(Consts.Computer.Cm2a.COMPANY)
                .build();
    }
    
    public static Computer newCm200() {
        return Computer.builder()
                .id(Consts.Computer.Cm200.ID)
                .name(Consts.Computer.Cm200.NAME)
                .introduced(Consts.Computer.Cm200.INTRODUCED)
                .discontinued(Consts.Computer.Cm200.DISCONTINUED)
                .company(Consts.Computer.Cm200.COMPANY)
                .build();
    }
    
    public static Computer newCm5e() {
        return Computer.builder()
                .id(Consts.Computer.Cm5e.ID)
                .name(Consts.Computer.Cm5e.NAME)
                .introduced(Consts.Computer.Cm5e.INTRODUCED)
                .discontinued(Consts.Computer.Cm5e.DISCONTINUED)
                .company(Consts.Computer.Cm5e.COMPANY)
                .build();
    }
    
    public static Computer newCm5() {
        return Computer.builder()
                .id(Consts.Computer.Cm5.ID)
                .name(Consts.Computer.Cm5.NAME)
                .introduced(Consts.Computer.Cm5.INTRODUCED)
                .discontinued(Consts.Computer.Cm5.DISCONTINUED)
                .company(Consts.Computer.Cm5.COMPANY)
                .build();
    }
    
    public static Computer newMackBookPro() {
        return Computer.builder()
                .id(Consts.Computer.MacBookPro.ID)
                .name(Consts.Computer.MacBookPro.NAME)
                .introduced(Consts.Computer.MacBookPro.INTRODUCED)
                .discontinued(Consts.Computer.MacBookPro.DISCONTINUED)
                .company(Consts.Computer.MacBookPro.COMPANY)
                .build();
    }
    
    public static Computer newAppleIIe() {
        return Computer.builder()
                .id(Consts.Computer.AppleIIe.ID)
                .name(Consts.Computer.AppleIIe.NAME)
                .introduced(Consts.Computer.AppleIIe.INTRODUCED)
                .discontinued(Consts.Computer.AppleIIe.DISCONTINUED)
                .company(Consts.Computer.AppleIIe.COMPANY)
                .build();
    }
    
    public static Computer newAppleIIc() {
        return Computer.builder()
                .id(Consts.Computer.AppleIIc.ID)
                .name(Consts.Computer.AppleIIc.NAME)
                .introduced(Consts.Computer.AppleIIc.INTRODUCED)
                .discontinued(Consts.Computer.AppleIIc.DISCONTINUED)
                .company(Consts.Computer.AppleIIc.COMPANY)
                .build();
    }
    
    public static Computer newAppleIIGS() {
        return Computer.builder()
                .id(Consts.Computer.AppleIIGS.ID)
                .name(Consts.Computer.AppleIIGS.NAME)
                .introduced(Consts.Computer.AppleIIGS.INTRODUCED)
                .discontinued(Consts.Computer.AppleIIGS.DISCONTINUED)
                .company(Consts.Computer.AppleIIGS.COMPANY)
                .build();
    }
    
    public static Computer newAppleIIcPlus() {
        return Computer.builder()
                .id(Consts.Computer.AppleIIcPlus.ID)
                .name(Consts.Computer.AppleIIcPlus.NAME)
                .introduced(Consts.Computer.AppleIIcPlus.INTRODUCED)
                .discontinued(Consts.Computer.AppleIIcPlus.DISCONTINUED)
                .company(Consts.Computer.AppleIIcPlus.COMPANY)
                .build();
    }
    
    public static Computer newAppleIIPlus() {
        return Computer.builder()
                .id(Consts.Computer.AppleIIPlus.ID)
                .name(Consts.Computer.AppleIIPlus.NAME)
                .introduced(Consts.Computer.AppleIIPlus.INTRODUCED)
                .discontinued(Consts.Computer.AppleIIPlus.DISCONTINUED)
                .company(Consts.Computer.AppleIIPlus.COMPANY)
                .build();
    }
    
    public static Computer newAppleIII() {
        return Computer.builder()
                .id(Consts.Computer.AppleIII.ID)
                .name(Consts.Computer.AppleIII.NAME)
                .introduced(Consts.Computer.AppleIII.INTRODUCED)
                .discontinued(Consts.Computer.AppleIII.DISCONTINUED)
                .company(Consts.Computer.AppleIII.COMPANY)
                .build();
    }

    public static List<Computer> allComputerInDatabase() {
        return Arrays.asList(newMacBookPro15(), newCm2a(), newCm200(), newCm5e(), newCm5(), newMackBookPro(),
                newAppleIIe(), newAppleIIc(), newAppleIIGS(), newAppleIIcPlus(), newAppleIIPlus(), newAppleIII());
    }

    public static List<Computer> allCompanyInDatabaseReverse() {
        List<Computer> computers = allComputerInDatabase();
        Collections.reverse(computers);
        return computers;
    }

    public static List<Computer> allAppleComputer() {
        return allComputerInDatabase()
                .stream()
                .filter(computer -> computer.getName().contains("Apple") ||
                        (computer.getCompany() != null && computer.getCompany().getId() == 1))
                .collect(Collectors.toList());
    }

    public static List<Computer> allAppleComputerSortedByName() {
        return allAppleComputer()
                .stream()
                .sorted(Comparator.comparing(Computer::getName))
                .collect(Collectors.toList());
    }
}
