package org.grorg.grordi.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public final class Consts {
    public static final long FIRST_ID_IN_DATABASE = 1;

    public final static class Company {
        public static final long TOTAL_NUMBER_IN_DATABASE = 2;

        public final static class Valid {
            public static final long ID = 1;
            public static final String NAME = "company";
        }

        public static final class Invalid {
            public static final long ID = 0;
            public static final String NAME = "";
        }

        public static final class Apple {
            public static final long ID = 1;
            public static final String NAME = "Apple Inc.";
        }

        public static final class ThinkingMachines {
            public static final long ID = 2;
            public static final String Name = "Thinking Machines";
        }
    }

    public static final class Computer {
        public static final long FIRST_ID_IN_DATABASE = 1;
        public static final long TOTAL_NUMBER_IN_DATABASE = 12;
        public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ISO_LOCAL_DATE;

        public static final class Valid {
            public static final long ID = 1;
            public static final String NAME = "computer";
            public static final LocalDate INTRODUCED = LocalDate.parse("1972-01-01");
            public static final LocalDate DISCONTINUED = LocalDate.parse("1984-01-01");
            public static final String INTRODUCED_STRING = INTRODUCED.format(DATE_FORMAT);
            public static final String DISCONTINUED_STRING = DISCONTINUED.format(DATE_FORMAT);
            public static final org.grorg.grordi.entity.Company COMPANY = CompanyFactory.newValidCompany();
        }

        public static final class Invalid {
            public static final long ID = -1;
            public static final String NAME = "";
            public static final String INTRODUCED_STRING = "01-30-1980";
            public static final String DISCONTINUED_STRING = "01-30-1980";
        }

        public static final class MacBookPro15 {
            public static final long ID = FIRST_ID_IN_DATABASE;
            public static final String NAME = "MacBook Pro 15.4 inch";
            public static final LocalDate INTRODUCED = null;
            public static final LocalDate DISCONTINUED = null;
            public static final org.grorg.grordi.entity.Company COMPANY = CompanyFactory.newAppleCompany();
        }

        public static final class Cm2a {
            public static final long ID = 2;
            public static final String NAME = "CM-2a";
            public static final LocalDate INTRODUCED = null;
            public static final LocalDate DISCONTINUED = null;
            public static final org.grorg.grordi.entity.Company COMPANY = CompanyFactory.newThinkingMachines();
        }

        public static final class Cm200 {
            public static final long ID = 3;
            public static final String NAME = "CM-200";
            public static final LocalDate INTRODUCED = null;
            public static final LocalDate DISCONTINUED = null;
            public static final org.grorg.grordi.entity.Company COMPANY = CompanyFactory.newThinkingMachines();
        }

        public static final class Cm5e {
            public static final long ID = 4;
            public static final String NAME = "CM-5e";
            public static final LocalDate INTRODUCED = null;
            public static final LocalDate DISCONTINUED = null;
            public static final org.grorg.grordi.entity.Company COMPANY = CompanyFactory.newThinkingMachines();
        }

        public static final class Cm5 {
            public static final long ID = 5;
            public static final String NAME = "CM-5";
            public static final LocalDate INTRODUCED = LocalDate.parse("1991-01-01");
            public static final LocalDate DISCONTINUED = null;
            public static final org.grorg.grordi.entity.Company COMPANY = CompanyFactory.newThinkingMachines();
        }

        public static final class MacBookPro {
            public static final long ID = 6;
            public static final String NAME = "MacBook Pro";
            public static final LocalDate INTRODUCED = LocalDate.parse("2006-01-10");
            public static final LocalDate DISCONTINUED = null;
            public static final org.grorg.grordi.entity.Company COMPANY = CompanyFactory.newAppleCompany();
        }

        public static final class AppleIIe {
            public static final long ID = 7;
            public static final String NAME = "Apple IIe";
            public static final LocalDate INTRODUCED = null;
            public static final LocalDate DISCONTINUED = null;
            public static final org.grorg.grordi.entity.Company COMPANY = null;
        }

        public static final class AppleIIc {
            public static final long ID = 8;
            public static final String NAME = "Apple IIc";
            public static final LocalDate INTRODUCED = null;
            public static final LocalDate DISCONTINUED = null;
            public static final org.grorg.grordi.entity.Company COMPANY = null;
        }

        public static final class AppleIIGS {
            public static final long ID = 9;
            public static final String NAME = "Apple IIGS";
            public static final LocalDate INTRODUCED = null;
            public static final LocalDate DISCONTINUED = null;
            public static final org.grorg.grordi.entity.Company COMPANY = null;
        }

        public static final class AppleIIcPlus {
            public static final long ID = 10;
            public static final String NAME = "Apple IIc Plus";
            public static final LocalDate INTRODUCED = null;
            public static final LocalDate DISCONTINUED = null;
            public static final org.grorg.grordi.entity.Company COMPANY = null;
        }

        public static final class AppleIIPlus {
            public static final long ID = 11;
            public static final String NAME = "Apple II Plus";
            public static final LocalDate INTRODUCED = null;
            public static final LocalDate DISCONTINUED = null;
            public static final org.grorg.grordi.entity.Company COMPANY = null;
        }

        public static final class AppleIII {
            public static final long ID = 12;
            public static final String NAME = "Apple III";
            public static final LocalDate INTRODUCED = LocalDate.parse("1980-05-01");
            public static final LocalDate DISCONTINUED = LocalDate.parse("1984-04-01");
            public static final org.grorg.grordi.entity.Company COMPANY = CompanyFactory.newAppleCompany();
        }
    }

    public static final class SearchCriteria {
        public static class Valid {
            public static class Orientation {
                public static final String ASCENDING =
                        org.grorg.grordi.dto.SearchCriteria.SORTING_ORIENTATION_ASCENDING;
                public static final String DESCENDING =
                        org.grorg.grordi.dto.SearchCriteria.SORTING_ORIENTATION_DESCENDING;
            }
        }

        public static final class Invalid {
            public static final int PAGE_SIZE = 1000;
            public static final int PAGE_SIZE_TO_LOW = 0;
            public static final int PAGE_INDEX = -1;
        }

        public static final class Computer {
            public static class Invalid {
                public static final String COLUMN = "INVALID";
            }
        }

        public static final class Company {
            public static class Invalid {
                public static final String COLUMN = "INVALID";
            }
        }
    }

    public final static class User {
        public final static class Valid {
            public static final String UID = "101201230";
        }

        public static final class Invalid {
            public static final String UID = "invalid";
        }

        public final static class Beni {
            public static final long ID = 1;
            public static final String UID = "105684152189918478870";
        }
    }
}
